const webpack = require('webpack');
const path = require('path');
const chalk = require('chalk');

// plugins 
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const FriendlyErrorsWebpackPlugin = require('friendly-errors-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const SimpleProgressPlugin = require('webpack-simple-progress-plugin');


const {
    root
} = require('./helpers');





const config = {};


config.stats = 'none';


config.entry = {
    app: root('src/app/app.js')
}


config.output = {
    path: root('dist'),
    filename: 'js/[name].js',
    publicPath: '/',
    devtoolModuleFilenameTemplate: '[absolute-resource-path]',
    devtoolFallbackModuleFilenameTemplate: '[absolute-resource-path]?[hash]'
};


config.resolve = {
    alias: {
        vue: 'vue/dist/vue.js',
        components: root('src/app/components'),
        '@': root('src/app'),
        api: root('src/app/store/api/api.js'),
        img: root('src/public/img'),
        fonts: root('src/public/fonts')
    },
    modules: [
        root('src/app'),
        'node_modules'
    ],
    extensions: ['.vue', '.js', '.json', '.pug', '.png', '.svg', '.jpg', '.*']
};



config.module = {};



config.module.rules = [
    {
        test: /\.(js|vue)$/,
        loader: 'eslint-loader',
        enforce: 'pre',
        include: [root('src')],
        options: {
            formatter: require('eslint-friendly-formatter')
        }
    },
    {
        test: /\.vue$/,
        use: [{ 
            loader: 'vue-loader',
         }]
    },
    {
        test: /\.js$/,
        use: 'babel-loader',
        exclude: /node_modules/
    },
    {
        test: /\.(png|jpe?g|gif|svg)$/,
        use: [{
            loader: 'file-loader',
            options: {
                name: 'img/[name].[ext]'
            }
        },
        {
            loader: 'image-webpack-loader',
            options: {
                pngquant: {
                    quality: '90',
                    speed: 4
                },
                mozjpeg: {
                    progressive: true,
                    quality: 70
                }
            }
        }
        ]
    },
    {
        test: /\.(woff|woff2|eot|ttf)(\?[a-z0-9=.]+)?$/,
        use: [
            {
                loader: 'file-loader',
                options: {
                    name: 'fonts/[name].[ext]'
                }
            }
        ]
    }
];



config.plugins = [
    new HtmlWebpackPlugin({
        template: root('src/public/index.html'),
        filename: 'index.html',
        inject: true
    }),
    new webpack.DefinePlugin({
        'NODE_ENV': JSON.stringify(process.env.NODE_ENV),
        '__DEV__': process.env.__DEV__,
        '__LOCAL__': process.env.__LOCAL__
    }),
    new webpack.ProvidePlugin({
        Vue: 'vue',
        Utils: 'utils',
        Component: ['vue-class-component', 'default'],
        router: ['router', 'default'],
        mapState: ['vuex', 'mapState'],
        mapActions: ['vuex', 'mapActions'],
        mapMutations: ['vuex', 'mapMutations'],
        mapGetters: ['vuex', 'mapGetters']
    }),
    new FriendlyErrorsWebpackPlugin(),
    new SimpleProgressPlugin({
        progressOptions: {
            clear: true
        }
    })
]






module.exports = config;