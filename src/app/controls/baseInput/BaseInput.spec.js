import BaseInput from './BaseInput.vue';
describe('Base input test', () => {
    const Component = Vue.extend(BaseInput);
    it('Should have value and id props', () => {
        const value = 'Passed value';
        const id = 'Passed id';
        const vm = new Component({
            propsData: {
                value,
                id
            }
        }).$mount();
        expect(vm.value).to.equal(value);
        expect(vm.id).to.equal(id);
    });

    it('Should trigger testable function of base input', () => {
        const vm = new Component().$mount();
        expect(vm.testable(123)).to.equal(123);
    });
});