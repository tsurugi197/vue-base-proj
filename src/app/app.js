import 'babel-polyfill';

import App from './components/App.vue';
import router from './pages/routing';
import store from './store/store';

import './controls/controls';
import './../styles/main.scss';

window.apiPath = '';
Vue.config.productionTip = false;

if (__DEV__) {
    window.apiPath = __LOCAL__ ? 'http://path-to-local-api/' : 'http://path-to-remote-api/';
} else {
    Vue.config.devtools = false;
    Vue.config.debug = false;
}

export const app = new Vue({
    el: '#root',
    router,
    store,
    provide() {
        return {
            utils: Utils
        };
    },
    render: h => h(App)
});

