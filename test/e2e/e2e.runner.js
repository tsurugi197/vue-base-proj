
const spawn = require('cross-spawn');
const runner = spawn('./node_modules/.bin/nightwatch', ['--config', 'test/e2e/nightwatch.js', '--env', 'chrome'], { stdio: 'inherit' });

runner.on('exit', code => {
    process.exit();
});

runner.on('error', err => {
    server.close();
    throw err;
});